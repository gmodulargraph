 /* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * gtkgraph-demo.c
 * Copyright (C) Nikita Zlobin 2011 <nick87720z@gmail.com>
 * 
 * gtkgraph is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * gtkgraph is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE

#include <goocanvas.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

static gboolean on_root_buttonPress_event   (GooCanvasItem * item, GooCanvasItem * target, GdkEventButton * event, gpointer data);
static gboolean on_root_buttonRelease_event (GooCanvasItem * item, GooCanvasItem * target, GdkEventButton * event, gpointer data);
static gboolean on_root_dragged             (GooCanvasItem * item, GooCanvasItem * target, GdkEventMotion * event, gpointer data);

static gboolean on_module_enterNotify_event (GooCanvasItem * item, GooCanvasItem * target, GdkEventCrossing * event, gpointer data);
static gboolean on_module_leaveNotify_event (GooCanvasItem * item, GooCanvasItem * target, GdkEventCrossing * event, gpointer data);

static gboolean on_module_notify_width  (GooCanvasItem * item, GParamSpec * event, gpointer data);
static gboolean on_module_notify_height (GooCanvasItem * item, GParamSpec * event, gpointer data);

static gboolean on_module_buttonPress_event   (GooCanvasItem * item, GooCanvasItem * target, GdkEventButton * event, gpointer data);
static gboolean on_module_buttonRelease_event (GooCanvasItem * item, GooCanvasItem * target, GdkEventButton * event, gpointer data);
static gboolean on_module_dragged             (GooCanvasItem * item, GooCanvasItem * target, GdkEventMotion * event, gpointer data);

static gboolean on_module_text_notify (GooCanvasItem * item, GParamSpec * event, gpointer data);
static gboolean on_port_text_notify   (GooCanvasItem * item, GParamSpec * event, gpointer data);
static gboolean on_port_size_notify (GooCanvasItem * item, GParamSpec * event, gpointer data);
static gboolean on_port_moved (GooCanvasItem * item, GParamSpec * event, gpointer data);

static gboolean wire_entered (GooCanvasItem * item, GooCanvasItem * target, GdkEventCrossing * event, gpointer data);
static gboolean wire_left    (GooCanvasItem * item, GooCanvasItem * target, GdkEventCrossing * event, gpointer data);
static gboolean on_wire_buttonPress_event (GooCanvasItem * item, GooCanvasItem * target, GdkEventButton * event, gpointer data);

double border_threshold = 1.0;

#define drag_button 1

typedef
struct {
    struct {
        unsigned color;
        float rounding;
        struct {
            unsigned threshold;
            unsigned border_color;
        } border;
        struct {
            unsigned shadow_color;
            float shadow_size;
        } shadow;
        struct {
            unsigned alignment;
            char * font;
        } caption;
    } moduleBox;
} GModularGraphStyle;

typedef
struct {
    GooCanvasItem * box;     /* Background */
    GooCanvasItem * outer_border;
    GooCanvasItem * inner_border;

    GtkOrientation * orientation;

    GooCanvasItem * caption;
    GooCanvasItem * group;   /* Just a logical container, without any look */
    GooCanvasItem * layout;  /* Used to organize elements (caption, ports) */

    /* List of ports groups */
    GList * ports;
    GList * connections;

    /* Dragging data */
    double drag_hold_x, drag_hold_y;
    unsigned drag_cb;
} GModularGraph_Module;

#define G_MODULAR_GRAPH_MODULE(p) ((GModularGraph_Module *) (p))

typedef enum {
    G_MODULAR_GRAPH_PORT_DIRECTION_IN = 1,
    G_MODULAR_GRAPH_PORT_DIRECTION_OUT
} GModularGraphPortDirection;

typedef
struct {
    GList * ports;
    GModularGraphPortDirection direction;

    unsigned width;
    unsigned p_count;
    GooCanvasItem * container;

    GModularGraph_Module * module;
} GModularGraphPortsGroup;

#define G_MODULAR_GRAPH_PORTS_GROUP(p) ((GModularGraphPortsGroup *) (p))

typedef
struct {
    GooCanvasItem * box;
    GooCanvasItem * text;
    GooCanvasItem * group;
    GModularGraphPortsGroup * ports_group;
    double min_width;

    GList * connections;
    double wire_x, wire_y;
    double direction; /* In deegreens. Also affects wire direction. */
} GModularGraph_Port;

#define G_MODULAR_GRAPH_PORT(p) ((GModularGraph_Port *) (p))

typedef
struct {
    GooCanvasItem * path_b;
    GooCanvasItem * path_t;
    GooCanvasItem * group;

    //char ** cmd_v;
    //unsigned * cmd_len;
    
    double x1, y1, cx1, cy1;
    double x2, y2, cx2, cy2;

    double angle1, angle2; /* In degreens */
    unsigned color;
    double blink;
    double highlight;
} GModularGraph_Wire;

#define G_MODULAR_GRAPH_WIRE(p) ((GModularGraph_Wire *) (p))

typedef
struct {
    GModularGraph_Wire * wire;
    GModularGraph_Port * port1;
    GModularGraph_Port * port2;
} GModularGraph_Connection;

#define G_MODULAR_GRAPH_CONNECTION(p) ((GModularGraph_Connection *) (p))

GModularGraph_Wire * g_modular_graph_wire_new (double x1, double y1, double angle1,
                                               double x2, double y2, double angle2,
                                               char * tooltip, unsigned color);

/* Canvas data */
GtkWidget * canvas;
GooCanvasItem * root_item;
static double rounding = 3;

static GModularGraph_Module * module[2] = {NULL, NULL};

void g_modular_graph_module_add_ports_group (GModularGraphPortsGroup ** ret, GModularGraph_Module * module, GModularGraphPortDirection direction);

static void canvas_text_size (GooCanvasItem * item, double * w, double * h)
{
  PangoRectangle rect;

  goo_canvas_text_get_natural_extents (GOO_CANVAS_TEXT (item), NULL, & rect);
  if (w) *w = (double) PANGO_PIXELS (rect.width);
  if (h) *h = (double) PANGO_PIXELS (rect.height);
}

static inline void
text_wanted_size (GooCanvasItem * item, double * w, double * h)
{
  canvas_text_size (item, w, h);
  if (w) *w += rounding * 2;
}

static void module_update_size (GModularGraph_Module * module)
{
  double w = 0, h = 0;
  double x = 0, y = 0;
  GooCanvasBounds bounds;

  g_object_get (module->layout, "x", & x, "y", & y, NULL);
  goo_canvas_item_get_bounds (module->layout, & bounds);
  w = bounds.x2 - bounds.x1 + x;
  h = bounds.y2 - bounds.y1 + y;

  double text_w, text_h;
  text_wanted_size (module->caption, & text_w, & text_h);
  if (w < text_w) w = text_w;
  h += rounding;
  w --; // Strangely, without this line right side of border is placed later by 1 pixel, relatively to output ports borders
  g_object_set (module->box, "width", w, "height", h, NULL);
}

GModularGraph_Module * g_modular_graph_module_new (GooCanvasItem * parent,
                                                   char * caption, char * tooltip,
                                                   int placing, int x, int y)
{
  GModularGraph_Module * module;
  unsigned width = 50, height = 20;

  /* Fundament */
  module = calloc (1, sizeof (GModularGraph_Module));
  module->group = goo_canvas_group_new (parent, "x", (double) x, "y", (double) y, "tooltip", tooltip, NULL);

  /* Decoration */
  module->outer_border = goo_canvas_rect_new (module->group,
                                              - border_threshold / 2,   - border_threshold / 2,
                                              width + border_threshold, height + border_threshold,
                                              "radius-x", rounding + border_threshold / 2, "radius-y", rounding + border_threshold / 2,
                                              "stroke-color-rgba", 0x0000005f,   NULL);

  module->box = goo_canvas_rect_new (module->group,
                                     border_threshold / 2,     border_threshold / 2,
                                     width - border_threshold, height - border_threshold,
                                     "radius-x", rounding,     "radius-y", rounding,
                                     "stroke-color", "gray45", "fill-color", "gray30",
                                     NULL);

  module->inner_border = goo_canvas_rect_new (module->group,
                                              border_threshold * 1.5,       border_threshold * 1.5,
                                              width - border_threshold * 3, height - border_threshold * 3,
                                              "radius-x", rounding - border_threshold / 2, "radius-y", rounding - border_threshold / 2,
                                              "stroke-color", "gray33",     NULL);

  g_signal_connect (G_OBJECT (module->box), "notify::width",  G_CALLBACK (on_module_notify_width),  module);
  g_signal_connect (G_OBJECT (module->box), "notify::height", G_CALLBACK (on_module_notify_height), module);

  /* Caption */
  module->caption = goo_canvas_text_new (module->group, "",
                                        rounding, 0, -1, GTK_ANCHOR_NORTH_WEST,
                                        "font", "sans 8",
                                        "fill-color", "white", NULL);
  g_signal_connect (G_OBJECT (module->caption), "notify::text", G_CALLBACK (on_module_text_notify), module);

  /* Predefined groups */
  module->layout = goo_canvas_table_new (module->group, "y", 15.0, "column-spacing", 5.0, NULL);
  g_modular_graph_module_add_ports_group (NULL, module, G_MODULAR_GRAPH_PORT_DIRECTION_IN);
  g_modular_graph_module_add_ports_group (NULL, module, G_MODULAR_GRAPH_PORT_DIRECTION_OUT);

  /* Dragging capabilities */
  g_signal_connect (G_OBJECT (module->group), "button-press-event",   G_CALLBACK (on_module_buttonPress_event),   module);
  g_signal_connect (G_OBJECT (module->group), "button-release-event", G_CALLBACK (on_module_buttonRelease_event), module);

  /* Mouse over */
  g_signal_connect (G_OBJECT (module->group), "enter-notify-event", G_CALLBACK (on_module_enterNotify_event), module);
  g_signal_connect (G_OBJECT (module->group), "leave-notify-event", G_CALLBACK (on_module_leaveNotify_event), module);

  module->connections = NULL;
  g_object_set (G_OBJECT (module->caption), "text", caption, NULL);
  return module;
}

void g_modular_graph_module_add_ports_group (GModularGraphPortsGroup ** ret, GModularGraph_Module * module, GModularGraphPortDirection direction)
{
  GModularGraphPortsGroup * group;
  GooCanvasItem * parent;

  if (direction == G_MODULAR_GRAPH_PORT_DIRECTION_IN || direction == G_MODULAR_GRAPH_PORT_DIRECTION_OUT)
       parent = module->layout;
  else parent = module->group;

  group = calloc (1, sizeof(GModularGraphPortsGroup));
  group->direction = direction;
  group->container = goo_canvas_table_new (parent, "x", 0.0, "y", 0.0, "row-spacing", 1.0, "column-spacing", 0.0,
                                           //"clip-path", "M 0 0 h 100 v 20 h -20 Z",
                                           //"horz-grid-line-width", 1.0, "vert-grid-line-width", 1.0,
                                           "stroke-color", "yellow", NULL);
  if (direction == G_MODULAR_GRAPH_PORT_DIRECTION_IN)
    goo_canvas_item_set_child_properties (module->layout, group->container, "column", 0, "x-align", 0.0, NULL);
  else if (direction == G_MODULAR_GRAPH_PORT_DIRECTION_OUT)
    goo_canvas_item_set_child_properties (module->layout, group->container, "column", 1, "x-align", 1.0, NULL);

  goo_canvas_item_set_child_properties (module->layout, group->container, "y-align", 0.0, NULL);

  module->ports = g_list_append (module->ports, group);
  group->module = module;

  if (ret) * ret = group;
}

static double ports_group_update_width (GModularGraphPortsGroup * group)
{
  double min_width = 0;
  GList * sib = group->ports;

  while (1)
	{
	  if (G_MODULAR_GRAPH_PORT (sib->data)->min_width > min_width)
		min_width = G_MODULAR_GRAPH_PORT (sib->data)->min_width;

	  if (! sib->next) break;
	  sib = sib->next;
	}
  sib = group->ports;
  while (1)
	{
	  g_object_set (G_MODULAR_GRAPH_PORT (sib->data)->box, "width", min_width, NULL);
	  if (! sib->next) break;
	  sib = sib->next;
	}
  return min_width;
}

void g_modular_graph_module_add_port (GModularGraph_Port ** ret,
                                      GModularGraph_Module * module,
                                      int direction,
                                      char * name, char * tooltip)
{
  /* Create ports group */
  GModularGraphPortsGroup * group = NULL;
  if (module->ports)
    {
      GList * elem = module->ports;
      while (1)
        {
          if (G_MODULAR_GRAPH_PORTS_GROUP (elem->data)->direction == direction)
            {
              group = elem->data;
              break;
            }
          if (! elem->next) break;
          elem = elem->next;
        }
    }
  if (! group)
  {
    g_modular_graph_module_add_ports_group (& group, module, direction);
  }

  GModularGraph_Port * port = calloc (1, sizeof (GModularGraph_Port));

  /* Decoration */
  port->box = goo_canvas_rect_new (group->container,
                                   0.5, 0.5, 50, 5,
                                   "tooltip", tooltip,
                                   //"radius-x", rounding,
                                   //"radius-y", rounding,
                                   "line-width", border_threshold,
                                   "stroke-color", "gray70",
                                   "fill-color", "gray60",
                                   NULL);
  g_signal_connect (G_OBJECT (port->box), "notify::width",  G_CALLBACK (on_port_size_notify), port);
  g_signal_connect (G_OBJECT (port->box), "notify::height", G_CALLBACK (on_port_size_notify), port);
  g_signal_connect (G_OBJECT (port->box), "notify::x", G_CALLBACK (on_port_moved), port);
  g_signal_connect (G_OBJECT (port->box), "notify::y", G_CALLBACK (on_port_moved), port);

  /* Caption */
  port->text = goo_canvas_text_new (group->container, "",
                                    rounding, 0, -1, GTK_ANCHOR_NORTH_WEST,
                                    "tooltip", tooltip,
                                    "font", "sans 8",
                                    "fill-color", "gray90", NULL);
  goo_canvas_item_set_child_properties (group->container, port->box,  "row", group->p_count, "x-expand", TRUE, "x-fill", TRUE, NULL);
  goo_canvas_item_set_child_properties (group->container, port->text, "row", group->p_count, "x-expand", TRUE, "x-fill", TRUE,
                                        "left-padding", rounding, "x-align", 0.5, NULL);

  g_signal_connect (G_OBJECT (port->text), "notify::text", G_CALLBACK (on_port_text_notify), port);

  /* Wire point */
  switch (direction)
    {
      case G_MODULAR_GRAPH_PORT_DIRECTION_IN:
        port->direction = 180.0; break;
      case G_MODULAR_GRAPH_PORT_DIRECTION_OUT:
        port->direction = 0.0; break;
    }
  
  /* Add port to group */
  port->ports_group = group;
  if (! g_list_find (group->ports, port))
    group->ports = g_list_append (group->ports, port);

  /* Complete initialization */
  g_object_set (G_OBJECT (port->text), "text", name, NULL);
  module_update_size (module);
  port->connections = NULL;

  group->p_count++;
  if (ret) *ret = port;
}

typedef
union {
    struct {
      uint8_t alpha;
      uint8_t blue;
      uint8_t green;
      uint8_t red;
    };
    uint32_t value;
} Color;

static void sort_uint8_ptr (uint8_t ** v, size_t size)
{
  uint8_t ** ptr, ** ptr_end;
  ptr_end = size - 1 + v;
  unsigned needs_repeat;

  do {
    needs_repeat = (0 == 0);
    ptr = v;
    while (ptr < ptr_end)
      {
        if (* ptr[0] > * ptr[1] )
          {
            uint8_t * tmp = ptr[0];            
            ptr[0] = ptr[1];
            ptr[1] = tmp;
          }
        else
          needs_repeat = (1 == 0);

        ptr++;
      }
    }
  while (needs_repeat);
}

void color_adjust_lightness (unsigned * value, double diff)
{
  Color color;
  color.value = * value;
  double mid_relative;
  double range;

  if (diff > 1.0)
    {
      uint8_t * color_c[3] = {& color.red, & color.green, & color.blue};
      sort_uint8_ptr ((void *)color_c, 3);

      range = * color_c[2] - * color_c[0];

      mid_relative = (range == 0.0) ? 0.0 : (* color_c[1] - * color_c[0]) / range;
      * color_c[0] = (* color_c[0] * diff > 255) ? 255 : * color_c[0] * diff;
      * color_c[2] = (* color_c[2] * diff > 255) ? 255 : * color_c[2] * diff;

      if (* color_c[0] == 255)
        * color_c[1] = 255;
      else
        * color_c[1] = * color_c[0] + (* color_c[2] - * color_c[0]) * mid_relative;
    }
  else
    {
      color.red *= diff;
      color.green *= diff;
      color.blue *= diff;
    }

  * value = color.value;
}

GModularGraph_Connection * g_modular_graph_connection_new (GModularGraph_Port * out, GModularGraph_Port * in)
{
  GModularGraph_Connection * connection = calloc (1, sizeof (GModularGraph_Connection));
  connection->port1 = out;
  connection->port2 = in;

  GooCanvasBounds p_bounds [2];
  goo_canvas_item_get_bounds (out->box, & p_bounds [0]);
  goo_canvas_item_get_bounds (in->box, & p_bounds [1]);
  connection->wire  = g_modular_graph_wire_new (p_bounds[0].x2, (p_bounds[0].y1 + p_bounds[0].y2) / 2, out->direction,
                                                p_bounds[1].x1, (p_bounds[1].y1 + p_bounds[1].y2) / 2, in->direction,
                                                "Test connection", 0x803030ff);

  GModularGraph_Module * module;
  module = out->ports_group->module;
  module->connections = g_list_append (module->connections, connection);
  out->connections = g_list_append (out->connections, connection);

  module = in->ports_group->module;
  module->connections = g_list_append (module->connections, connection);
  in->connections = g_list_append (in->connections,  connection);

  g_signal_connect (G_OBJECT (connection->wire->group), "button-release-event",
                    G_CALLBACK (on_wire_buttonPress_event), connection->wire->group);
  return NULL;
}

GModularGraph_Wire * g_modular_graph_wire_new (double x1, double y1, double angle1, double x2, double y2, double angle2, char * tooltip, unsigned color)
{
  char * wire_data;
  GModularGraph_Wire * wire;
  wire = calloc (1, sizeof (GModularGraph_Wire));
  wire->x1 = x1, wire->y1 = y1;
  wire->x2 = x2, wire->y2 = y2;
  wire->angle1 = angle1;
  wire->angle2 = angle2;
  wire->color = color;
  wire->blink = 1.5;
  wire->highlight = 1.5;

  wire->cx1 = wire->x1 + cos (wire->angle1 * G_PI / 180) * 50;
  wire->cy1 = wire->y1 + sin (wire->angle1 * G_PI / 180) * 50;
  wire->cx2 = wire->x2 + cos (wire->angle2 * G_PI / 180) * 50;
  wire->cy2 = wire->y2 + sin (wire->angle2 * G_PI / 180) * 50;
  asprintf (& wire_data, "M%i,%i C%i,%i %i,%i %i,%i",
            (int) wire->x1, (int) wire->y1,
            (int) wire->cx1, (int) wire->cy1,
            (int) wire->cx2, (int) wire->cy2,
            (int) wire->x2, (int) wire->y2);

  wire->group = goo_canvas_group_new (root_item);

  unsigned color_b = color;
  color_adjust_lightness (& color_b, 1 / wire->blink);
  wire->path_b = goo_canvas_path_new (wire->group,
                                      wire_data,
                                      "line-width", 3.0,
                                      "stroke-color-rgba", color_b,
                                      "line-cap", CAIRO_LINE_CAP_BUTT, NULL);

  unsigned color_t = color;
  color_adjust_lightness (& color_t, wire->blink);
  wire->path_t = goo_canvas_path_new (wire->group,
                                      wire_data,
                                      "line-width", 1.5,
                                      "stroke-color-rgba", color_t,
                                      "line-cap", CAIRO_LINE_CAP_BUTT, NULL);

  g_signal_connect (G_OBJECT (wire->group), "enter-notify-event", G_CALLBACK (wire_entered), wire);
  g_signal_connect (G_OBJECT (wire->group), "leave-notify-event", G_CALLBACK (wire_left),    wire);
  g_object_set (wire->group, "tooltip", tooltip, NULL);

  return wire;
}

static gboolean after_init (gpointer data)
{
  goo_canvas_item_remove (G_MODULAR_GRAPH_PORT (data)->box);
  goo_canvas_item_remove (G_MODULAR_GRAPH_PORT (data)->text);
  return TRUE;
}

int main( int argc, char ** argv )
{
  GtkWidget * win;
  GtkWidget * scroller;

  gtk_init (& argc, & argv);
  win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  g_signal_connect (G_OBJECT (win), "delete-event", G_CALLBACK (gtk_main_quit), NULL);

  scroller = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scroller), GTK_SHADOW_IN);
  gtk_container_add (GTK_CONTAINER (win), scroller);

  /* Create canvas widget */
  canvas = goo_canvas_new ();
  gtk_widget_set_size_request (canvas, 600, 450);
  goo_canvas_set_bounds (GOO_CANVAS (canvas), 0, 0, 1000, 1000);
  g_object_set (G_OBJECT (canvas), "automatic-bounds", TRUE, NULL);

  gtk_container_add (GTK_CONTAINER (scroller), canvas);

  /* Root */
  root_item = goo_canvas_get_root_item (GOO_CANVAS (canvas));
  g_object_set (G_OBJECT (root_item), "antialias", CAIRO_ANTIALIAS_SUBPIXEL, NULL);
  g_object_set (G_OBJECT (root_item), "line-join", CAIRO_LINE_JOIN_ROUND, NULL);
  g_object_set (G_OBJECT (root_item), "line-cap",  CAIRO_LINE_CAP_ROUND, NULL);
  g_object_set (G_OBJECT (root_item), "line-width", border_threshold, NULL);
  g_signal_connect (G_OBJECT (root_item), "button-press-enter", G_CALLBACK (on_root_buttonPress_event),   NULL);
  g_signal_connect (G_OBJECT (root_item), "button-press-enter", G_CALLBACK (on_root_buttonRelease_event), NULL);

  GModularGraph_Port * port1, * port2;
  module[0] = g_modular_graph_module_new (root_item, "Module 1", "Module 1 tip", 0, 50, 50);
  g_modular_graph_module_add_port (NULL, module[0], G_MODULAR_GRAPH_PORT_DIRECTION_IN, "in-1", "Port 1 tip");
  g_modular_graph_module_add_port (& port2, module[0], G_MODULAR_GRAPH_PORT_DIRECTION_IN, "in-2", "Port 2 tip");
  module[1] = g_modular_graph_module_new (root_item, "Module 2", "Module 2 tip", 0, 200, 10);
  g_modular_graph_module_add_port (NULL, module[1], G_MODULAR_GRAPH_PORT_DIRECTION_IN, "in-1", "Port 1 tip");
  g_modular_graph_module_add_port (NULL, module[1], G_MODULAR_GRAPH_PORT_DIRECTION_IN, "in-2", "Port 2 tip");
  g_modular_graph_module_add_port (NULL, module[1], G_MODULAR_GRAPH_PORT_DIRECTION_IN, "input-3", "Port 3 tip");
  g_modular_graph_module_add_port (& port1, module[1], G_MODULAR_GRAPH_PORT_DIRECTION_OUT, "out-1", "Out 1 tip");
  g_modular_graph_module_add_port (NULL, module[1], G_MODULAR_GRAPH_PORT_DIRECTION_OUT, "out-2", "Out 2 tip");

  /* 2. Connection */
  g_modular_graph_wire_new (50, 100, 0.0, 300, 300, 180, "Test wire", 0x5f0000ff);
  g_modular_graph_wire_new (50, 300, 0.0, 300, 100, 180, "Test wire", 0x5f0000ff);

  g_modular_graph_connection_new (port1, port2);

  //g_timeout_add (3000, after_init, myport);

  gtk_widget_set_has_tooltip (canvas, TRUE);

  gtk_widget_show_all (win);
  gtk_main ();
  return 0;
}

/* Helpers (nothing yet) */

/* Root callbacks */

static gboolean on_root_buttonPress_event (GooCanvasItem * item, GooCanvasItem * target, GdkEventButton * event, gpointer data)
{
  return FALSE;
}

static gboolean on_root_buttonRelease_event (GooCanvasItem * item, GooCanvasItem * target, GdkEventButton * event, gpointer data)
{
  return FALSE;
}

static gboolean on_root_dragged (GooCanvasItem * item, GooCanvasItem * target, GdkEventMotion * event, gpointer data)
{
  return FALSE;
}

/* Module callbacks */

static gboolean on_module_enterNotify_event (GooCanvasItem * item, GooCanvasItem * target, GdkEventCrossing * event, gpointer data)
{
  char
    * inner_border_color = "gray40",
    * outer_border_color = "gray70";
  g_object_set (G_MODULAR_GRAPH_MODULE (data)->inner_border, "stroke-color", inner_border_color, NULL);
  g_object_set (G_MODULAR_GRAPH_MODULE (data)->box,          "stroke-color", outer_border_color, NULL);
  return FALSE;
}

static gboolean on_module_leaveNotify_event (GooCanvasItem * item, GooCanvasItem * target, GdkEventCrossing * event, gpointer data)
{
  char
    * inner_border_color = "gray33",
    * outer_border_color = "gray45";
  g_object_set (G_MODULAR_GRAPH_MODULE (data)->inner_border, "stroke-color", inner_border_color, NULL);
  g_object_set (G_MODULAR_GRAPH_MODULE (data)->box,          "stroke-color", outer_border_color, NULL);
  return FALSE;
}

static gboolean on_module_notify_width (GooCanvasItem * item, GParamSpec * event, gpointer data)
{
  double w;
  g_object_get (G_MODULAR_GRAPH_MODULE (data)->box,          "width", & w, NULL);
  g_object_set (G_MODULAR_GRAPH_MODULE (data)->inner_border, "width", w - border_threshold * 2, NULL);
  g_object_set (G_MODULAR_GRAPH_MODULE (data)->outer_border, "width", w + border_threshold * 2, NULL);
  return FALSE;
}

static gboolean on_module_notify_height (GooCanvasItem * item, GParamSpec * event, gpointer data)
{
  double h;
  g_object_get (G_MODULAR_GRAPH_MODULE (data)->box,          "height", & h, NULL);
  g_object_set (G_MODULAR_GRAPH_MODULE (data)->inner_border, "height", h - border_threshold * 2, NULL);
  g_object_set (G_MODULAR_GRAPH_MODULE (data)->outer_border, "height", h + border_threshold * 2, NULL);
  return FALSE;
}

#define DRAG_CB( mod )     (G_MODULAR_GRAPH_MODULE( mod )->drag_cb)
#define DRAG_HOLD_X( mod ) (G_MODULAR_GRAPH_MODULE( mod )->drag_hold_x)
#define DRAG_HOLD_Y( mod ) (G_MODULAR_GRAPH_MODULE( mod )->drag_hold_y)

static gboolean drag_redraw = TRUE;
static unsigned drag_redraw_timeout_id = 0;

static gboolean drag_redraw_timeout (gpointer data)
{
  drag_redraw = TRUE;
  return TRUE;
}

static
gboolean on_module_buttonPress_event (GooCanvasItem * item, GooCanvasItem * target, GdkEventButton * event, gpointer data)
{
  if (event->button == drag_button)
    {
      g_object_get (G_OBJECT (item),
                    "x", & DRAG_HOLD_X (data),
                    "y", & DRAG_HOLD_Y (data), NULL);
      DRAG_HOLD_X (data) = event->x - DRAG_HOLD_X (data);
      DRAG_HOLD_Y (data) = event->y - DRAG_HOLD_Y (data);

      DRAG_CB (data) = g_signal_connect (G_OBJECT (root_item), "motion-notify-event", G_CALLBACK (on_module_dragged), data);
      drag_redraw_timeout_id = g_timeout_add (30, drag_redraw_timeout, NULL);
    }
  return FALSE;
}

static
gboolean on_module_buttonRelease_event (GooCanvasItem * item, GooCanvasItem * target, GdkEventButton * event, gpointer data)
{
  if (event->button == drag_button)
    {
      g_source_remove (drag_redraw_timeout_id);
      drag_redraw_timeout_id = 0;
      if (DRAG_CB (data))
          g_signal_handler_disconnect (G_OBJECT (root_item), DRAG_CB (data));
    }
  return FALSE;
}

static
gboolean on_module_dragged (GooCanvasItem * item, GooCanvasItem * target, GdkEventMotion * event, gpointer data)
{
  if (drag_redraw)
    {
      g_object_set (G_MODULAR_GRAPH_MODULE (data)->group, "x", event->x - DRAG_HOLD_X (data), NULL);
      g_object_set (G_MODULAR_GRAPH_MODULE (data)->group, "y", event->y - DRAG_HOLD_Y (data), NULL);
      drag_redraw = FALSE;
    }
  return FALSE;
}

static gboolean on_module_text_notify (GooCanvasItem * item, GParamSpec * event, gpointer data)
{
  double w, h;
  text_wanted_size (item, &w, &h);

  g_object_set (G_MODULAR_GRAPH_MODULE (data)->box, "width",  w, NULL);
  g_object_set (G_MODULAR_GRAPH_MODULE (data)->box, "height", h, NULL);

  return FALSE;
}

#undef DRAG_CB
#undef DRAG_HOLD_X
#undef DRAG_HOLD_Y

/* Port callbacks */

static gboolean on_port_text_notify (GooCanvasItem * item, GParamSpec * event, gpointer data)
{
  double w, h;
  text_wanted_size (item, &w, &h);
  if (G_MODULAR_GRAPH_PORT (data)->min_width == w) return FALSE;

  G_MODULAR_GRAPH_PORT (data)->min_width = w;  
  ports_group_update_width (G_MODULAR_GRAPH_PORT (data)->ports_group);
  g_object_set (G_MODULAR_GRAPH_PORT (data)->box, "height", h, NULL);

  return FALSE;
}

static void port_update_wire_pos (GModularGraph_Port * port)
{
  double w, h;
  g_object_get (port->box, "width", & w, "height", & h, NULL);

  port->wire_y = h / 2;
  if (port->direction == 0.0)
    port->wire_x = w;
  else if (port->direction == 180.0)
    port->wire_x = 0.0;
}

static gboolean on_port_size_notify (GooCanvasItem * item, GParamSpec * event, gpointer data)
{
  g_print ("on_port_size_notify: %s\n", event->name);
  port_update_wire_pos (data);
  return FALSE;
}

static gboolean on_port_moved (GooCanvasItem * item, GParamSpec * event, gpointer data)
{
  g_print ("on_port_moved: %s\n", event->name);
  return FALSE;
}

#define DRAG_CB( mod )     (G_MODULAR_GRAPH_PORT( mod )->drag_cb)
#define DRAG_HOLD_X( mod ) (G_MODULAR_GRAPH_PORT( mod )->drag_hold_x)
#define DRAG_HOLD_Y( mod ) (G_MODULAR_GRAPH_PORT( mod )->drag_hold_y)

/* Wire callbacks */

static
gboolean wire_entered (GooCanvasItem    *item,
                       GooCanvasItem    *target,
                       GdkEventCrossing *event,
                       gpointer          data)
{
  unsigned color = G_MODULAR_GRAPH_WIRE (data)->color;
  color_adjust_lightness (& color, G_MODULAR_GRAPH_WIRE (data)->blink * G_MODULAR_GRAPH_WIRE (data)->highlight);

  g_object_set (G_MODULAR_GRAPH_WIRE (data)->path_t, "stroke-color-rgba", color, NULL);
  return FALSE;
}

static
gboolean wire_left (GooCanvasItem    *item,
                    GooCanvasItem    *target,
                    GdkEventCrossing *event,
                    gpointer          data)
{
  unsigned color = G_MODULAR_GRAPH_WIRE (data)->color;
  color_adjust_lightness (& color, G_MODULAR_GRAPH_WIRE (data)->blink);

  g_object_set (G_MODULAR_GRAPH_WIRE (data)->path_t, "stroke-color-rgba", color, NULL);
  return FALSE;
}

static
gboolean on_wire_buttonPress_event (GooCanvasItem * item, GooCanvasItem * target, GdkEventButton * event, gpointer data)
{
  if (event->button == 2)
    {
      g_print ("Wire was clicked\n");
    }
  return FALSE;
}
